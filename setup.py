#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

setup(
    # placeholder
    install_requires=[
        "Django>=1.2",
    ],
)
